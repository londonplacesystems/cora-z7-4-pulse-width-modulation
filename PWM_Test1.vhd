----------------------------------------------------------------------------------
-- Company: Londonplace Systems
-- Engineer: Glen Field
-- 
-- Create Date: 06/16/2019 08:56:04 PM
-- Design Name: 
-- Module Name: PWM_Test1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PWM_Test1 is
--  Port ( );
end PWM_Test1;

architecture Behavioral of PWM_Test1 is

component System_1_wrapper
Port (mid : out STD_LOGIC;
      pwm_out : out STD_LOGIC;
      resetn : in STD_LOGIC;
      sys_clock : in STD_LOGIC);
end component;

SIGNAL pwm_out : STD_LOGIC := '0';
SIGNAL mid : STD_LOGIC := '0';
SIGNAL resetn : STD_LOGIC := '1';
SIGNAL sys_clock : STD_LOGIC := '0';

begin

    UUT: System_1_wrapper port map (pwm_out => pwm_out, mid => mid, resetn => resetn, sys_clock => sys_clock);

    -- Specify 125 MHz clock.
    sys_clock <= not sys_clock after 4 ns;
    
    -- Define behaiour of the reset line.
    process
    begin
        resetn <= '1';
        wait for 100 ns;
        resetn <= '0';
        wait for 100 ns;
        resetn <= '1';
        wait;
    end process;
    

end Behavioral;
