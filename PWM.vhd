----------------------------------------------------------------------------------
-- Company: London Place Systems
-- Engineer: Glen Field
-- 
-- Create Date: 06/16/2019 04:21:16 PM
-- Design Name: 
-- Module Name: PWM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PWM is
    Generic (width: integer := 14);
    Port ( resetn : in STD_LOGIC;                           -- Active low synchronous reset
           clk : in STD_LOGIC;
           Q : in STD_LOGIC_VECTOR (width-1 downto 0);      -- PWM counter
           Rleft : in STD_LOGIC_VECTOR (width-2 downto 0);  -- Left switch point
           Rright : in STD_LOGIC_VECTOR (width-2 downto 0); -- Right switch point
           ce : out STD_LOGIC;                              -- Clock enable
           clr : out STD_LOGIC;                             -- Clear signal
           mid : out STD_LOGIC;                             -- Midpoint trigger
           pwm_out : out STD_LOGIC);                        -- PWM signal
end PWM;

architecture Behavioral of PWM is

    -- Signal declaration.
    SIGNAL pwm_sig : STD_LOGIC;
    SIGNAL clr_sig : STD_LOGIC;
    SIGNAL ce_sig : STD_LOGIC;
    SIGNAL Rleft_sig : STD_LOGIC_VECTOR(width-2 downto 0) := (others => '1');
    SIGNAL Rright_sig : STD_LOGIC_VECTOR(width-2 downto 0) := (others => '1');
    CONSTANT center : STD_LOGIC_VECTOR(width-1 downto 0) := ('1', others => '0');

begin

    -- Process to generate the PWM signal.
    process(clk, resetn)
    begin
        if (resetn = '0') then
            clr_sig <= '1';
            ce_sig <= '0';
            pwm_sig <= '0';
	    Rleft_sig <= Rleft;
            Rright_sig <= Rright;
        elsif rising_edge(clk) then
            clr_sig <= '0';
            ce_sig <= '1';
            -- Compare value depends on left or right of PWM carried sawtooth.
            if Q(width-1) = '0' then
                if Q(width-2 downto 0) > Rleft_sig then
                    pwm_sig <= '1';
                else
                    pwm_sig <= '0';
                end if;
            else
                if Q(width-2 downto 0) < Rright_sig then
                    pwm_sig <= '1';
                else
                    pwm_sig <= '0';
                end if;
            end if;
	    -- Latch magnitudes at PWM center.
            if Q = center then
                Rleft_sig <= Rleft;
                Rright_sig <= Rright;
            end if;
        end if;
    end process;

              
    -- Attach outputs.
    pwm_out <= pwm_sig;
    clr <= clr_sig;
    ce <= ce_sig;
    mid <= Q(width-1);


end Behavioral;
